# rename-files

Small command line utility to rename files to
[kebab case](https://en.wikipedia.org/wiki/Letter_case#Kebab_case) and replace German umlauts.

## Usage

```bash
$ rename-files -h
rename all files in a directory

Usage: rename-files [OPTIONS] [DIR]

Arguments:
  [DIR]  Directory holding the files to rename. The current directory is chosen by default

Options:
  -f, --force    Don't ask before renaming files
  -h, --help     Print help information
  -V, --version  Print version information
```

### Example

```bash
$ rename-files examples/
The following files will be renamed:

    examples/IPCC_AR6_WGI_Full_Report.pdf -> ipcc-ar-6-wgi-full-report.pdf
    examples/.hidden -> .hidden
    examples/Fira Code Bold.ttf -> fira-code-bold.ttf
    examples/FernwärmeErzeugung2018.csv -> fernwaerme-erzeugung-2018.csv

Continue? (y|[n])
```

## Install

```bash
git clone https://codeberg.org/daschw/rename-files
cargo install rename-files
```
