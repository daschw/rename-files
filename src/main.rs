use clap::Parser;
use colored::Colorize;
use convert_case::{Case, Casing};
use regex::Regex;
use std::path::{Path, PathBuf};
use std::str;
use std::{env, fs};
use text_io::read;

/// rename all files in a directory.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Directory holding the files to rename. The current directory is chosen by default.
    dir: Option<String>,
    /// Don't ask before renaming files
    #[arg(short, long, action = clap::ArgAction::SetTrue)]
    force: bool,
    /// Photo mode - use time stamp as file name
    #[arg(short, long, action = clap::ArgAction::SetTrue)]
    photo: bool,
    /// String to append to duplicates
    #[arg(short, long, default_value_t = String::from("-dup"))]
    append_to_duplicates: String,
}

fn main() -> std::io::Result<()> {
    let args = Args::parse();
    let dir = match args.dir {
        Some(p) => Path::new(&p).to_path_buf(),
        None => env::current_dir().unwrap(),
    };
    if dir.is_dir() {
        let old_names: Vec<PathBuf> = dir.read_dir().unwrap().map(|f| f.unwrap().path()).collect();
        let mut new_names: Vec<PathBuf> = old_names
            .iter()
            .map(|f| {
                if args.photo {
                    rename_photo(f)
                } else {
                    rename(f)
                }
            })
            .collect();
        if !args.append_to_duplicates.is_empty() {
            append_to_duplicates(&mut new_names, &args.append_to_duplicates)
        }
        if args.force || rename_dialog(&old_names, &new_names) {
            for i in 0..old_names.len() {
                fs::rename(&old_names[i], &new_names[i])?;
            }
        }
    }
    Ok(())
}

/// Print old and new file names and ask for confirmation
fn rename_dialog(old_names: &[PathBuf], new_names: &[PathBuf]) -> bool {
    println!("The following files will be renamed:");
    println!();
    for i in 0..old_names.len() {
        println!(
            "    {}{}{} -> {}",
            old_names[i].parent().unwrap().to_str().unwrap(),
            std::path::MAIN_SEPARATOR,
            old_names[i].file_name().unwrap().to_str().unwrap().red(),
            new_names[i].file_name().unwrap().to_str().unwrap().green()
        )
    }
    println!();
    println!("Continue? (y|[n])");
    let input: String = read!();
    matches!(input.to_lowercase().as_str(), "y")
}

fn rename(file: &Path) -> PathBuf {
    let p = file.parent().unwrap();
    let f = p.join(rename_stem(file.file_stem().unwrap().to_str().unwrap()));
    return match file.extension() {
        Some(ext) => f.with_extension(ext),
        None => f,
    };
}

fn rename_photo(file: &Path) -> PathBuf {
    if !file.is_file() {
        return PathBuf::from(file);
    }
    let opened = std::fs::File::open(file).unwrap();
    let mut bufreader = std::io::BufReader::new(opened);
    let exifreader = exif::Reader::new();
    match exifreader.read_from_container(&mut bufreader) {
        Ok(exif) => match exif.get_field(exif::Tag::DateTimeOriginal, exif::In::PRIMARY) {
            Some(created) => {
                let p = file.parent().unwrap();
                let f = p.join(created.display_value().to_string().replace(' ', "T"));
                match file.extension() {
                    Some(ext) => f.with_extension(ext),
                    None => f,
                }
            }
            _ => PathBuf::from(file),
        },
        _ => PathBuf::from(file),
    }
}

fn rename_stem(str: &str) -> String {
    let mut ret = str.to_string().to_case(Case::Kebab);
    let rules = [
        ("&", "and"),
        ("ä", "ae"),
        ("ö", "oe"),
        ("ü", "ue"),
        ("ß", "ss"),
    ];
    for rule in rules.iter() {
        let (from, to) = rule;
        ret = ret.replace(from, to);
    }
    let splits = Regex::new("[^A-Za-z0-9.]+").unwrap();
    ret = splits.replace_all(&ret, "-").to_string();
    return ret.trim_matches('-').to_string();
}

fn append_to_duplicates(names: &mut [PathBuf], appendix: &str) {
    for i in 0..(names.len() - 1) {
        for j in (i + 1)..names.len() {
            if names[i] == names[j] {
                let p = names[i].parent().unwrap();
                let mut new_stem = names[i].file_stem().unwrap().to_str().unwrap().to_string();
                new_stem.push_str(appendix);
                let new_name = p.join(&new_stem);
                names[j] = match names[i].extension() {
                    Some(ext) => new_name.with_extension(ext),
                    _ => new_name,
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        assert_eq!(
            rename(Path::new("Hello, world!")),
            PathBuf::from("hello-world")
        );
        assert_eq!(rename_stem("Hello, world!"), "hello-world");
        assert_eq!(rename_stem(".hidden"), ".hidden");
        assert_eq!(
            rename_stem("FernwärmeErzeugung2018.csv"),
            "fernwaerme-erzeugung-2018.csv"
        );
        assert_eq!(rename_stem("Fira Code Bold.ttf"), "fira-code-bold.ttf");
        assert_eq!(
            rename_stem("IPCC_AR6_WGI_Full_Report.pdf"),
            "ipcc-ar-6-wgi-full-report.pdf"
        );
    }
}
